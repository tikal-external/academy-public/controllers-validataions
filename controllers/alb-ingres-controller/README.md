# Alb ingress controller

- installed via [eks-addons terraform module](https://github.com/aws-ia/terraform-aws-eks-blueprints/blob/main/examples/eks-cluster-with-external-dns/main.tf#L100)
    - ALB ingress controller [documentation](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/)

- manually added tags to subnets (private-subnets , public-subnets) depends on the cluster tenancy does it have public subnets or not.

- test alb-ingress-controller

```sh
apply -k ./controllers/alb-ingres-controller/
```

should yield:

```sh
service/web-test created
deployment.apps/web-test created
ingress.networking.k8s.io/web-test created
```

- check in AWS console the loadbalancer status
    Check the loadBlancer has a tag named:
    `elbv2.k8s.aws/cluster` with values of the cluster name e.g. `algo-saas-prod`

    ![img](https://i.imgur.com/CgMhwla.png)
- 