# external-dns controller

- installed via [eks-addons terraform module](https://github.com/aws-ia/terraform-aws-eks-blueprints/tree/main/examples/eks-cluster-with-external-dns)
    - [Official Docs](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/integrations/external_dns/)

test scnario:

1. ingress resource with annotaion like so:
    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      annotations:
        external-dns.alpha.kubernetes.io/hostname: ing-test.test.vesttoo.com
    ...
    ```

1. checkn in aws that the record was created by the controller automatically [link](https://us-east-1.console.aws.amazon.com/route53/v2/hostedzones?region=us-east-1&skipRegion=true#ListRecordSets/Z07010633UAR31TIBSEVC)


![img](https://i.imgur.com/olS4MdD.png)