# external-secrets-operator

- installed via [eks-addons terraform module](https://github.com/aws-ia/terraform-aws-eks-blueprints/tree/main/examples/external-secrets)
    - [Official Docs](https://external-secrets.io/v0.5.9/)

test scnario:

![kroki](https://kroki.io/blockdiag/svg/eNp1jc0KwjAQhO95imFPevAJ_AER70IOPYiHNVlCMU1km4IgvrutBVGL15lvvjnH7C6-5oC7AWhbWRxYubElqxBWiw3IilMprwRmlriR9spO_HzoYWh_K6KJ48jR8q9oF7u2R799bgyr2suvEG-jMVPl0eWYFWtQVk5B6NQ_T7YfWFCRNFCPJ3mWTaw=)

<!-- 
blockdiag {
  "AWS ParamStore" <-> "SecretStore 
(namespaced)" <- 
"ExternalSecret";
  "AWS ParamStore" <-> "ClusterSecretStore 
(clusterWide)" <- 
"External Secret";


 "AWS ParamStore" [color = "orange"];
 "External Secret" [color = "green"];
}
 -->

1. Create paramater in SSM

```json
{
  "name": {"first": "Tom", "last": "Anderson"},
  "friends": [
    {"first": "Dale", "last": "Murphy"},
    {"first": "Roger", "last": "Craig"},
    {"first": "Jane", "last": "Murphy"}
  ]
}
```

1. Create credStore yaml and store it in this folder

```yml
apiVersion: external-secrets.io/v1beta1
kind: SecretStore
metadata:
  name: secretstore-for-test
spec:
  provider:
    aws:
      service: ParameterStore
      region: us-east-1
      auth:
        jwt:
          serviceAccountRef:
            name: external-secrets-sa 
```

1. Create externalSecrest yaml named `es-test` and store it in this folder

```yml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: tikal-test-es
spec:
  refreshInterval: 1m
  secretStoreRef:
    name: secretstore-for-test
    kind: SecretStore
  target:
    name: tikal-test-secret
    creationPolicy: Owner
  data:
  - secretKey: firstname
    remoteRef:
      key: /tikal/test-praram
      property: name.first # Tom
  - secretKey: first_friend
    remoteRef:
      key: /tikal/test-praram
      property: friends.1.first # Roger
  - secretKey: zero_friend
    remoteRef:
      key: /tikal/test-praram
      property: friends.0.first # Dale
```

1. Test that you have a secret named `tikal-test-secret` with the key value as stored in param store

```sh
kubectl get secret tikal-test-secret -o json | jq '.data | map_values(@base64d)'
```

should yield:

```json
{
  "first_friend": "Roger",
  "firstname": "Tom",
  "zero_friend": "Dale"
}
```

